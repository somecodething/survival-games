package org.MC3.sg;

import java.util.ArrayList;
import java.util.HashMap;

import org.MC3.sg.world.Maps;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Vars {
	public static String prefix = "§3[SG]: §a";
	
	public static Maps map = null;
	
	public static String nextEvent = "Chest Refill";
	
	public static boolean ingame = false;
	public static boolean starting = false;
	public static boolean canStart = true;

	public static long startTime = 0;
	
	public static ArrayList<Player> participants = new ArrayList<Player>();
	public static ArrayList<Player> GodMode = new ArrayList<Player>();
	public static ArrayList<Location> openedChests = new ArrayList<Location>();
	
	public static HashMap<Player, Player> lastDamager = new HashMap<Player, Player>();
	public static HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	
	public static int deaths = 0;
	public static int requiredPlayerCount = 2;
	
	public static double tier2Chance = 0.2;
}
